# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
import pysolr
import json
import pymysql
import ConfigParser


# Create your views here.
__author__ = 'Chandler Huang <previa@gmail.com>'

notation = 100000

def load_cause_dict():

    cause_dict = {
        'c1': u"腸道感染症",
        'c2': u"結核病",
        'c3': u"敗血症",
        'c6': u"惡性腫瘤",
        'c8': u"貧血",
        'c9': u"糖尿病",
        'c15': u"高血壓性疾病",
        'c16': u"心臟疾病（高血壓性疾病除外）",
        'c17': u"腦血管疾病",
        'c18': u"動脈粥樣硬化",
        'c22': u"急性支氣管炎及急性細支氣管炎",
        'c28': u"慢性肝病及肝硬化",
        'c32': u"腎炎、腎病症候群及腎病變",
        'c38': u"事故傷害",
        'c39': u"蓄意自我傷害（自殺）",
        'c40': u"加害（他殺）",
        'c41': u"其他",
    }

    return cause_dict


def get_config():
    config_filename = "settings_local"
    config = ConfigParser.ConfigParser()
    config.readfp(open(config_filename))
    conf = {}
    conf['mysql_host'] = config.get('Mysql', 'host')
    conf['mysql_port'] = int(config.get('Mysql', 'port'))
    conf['mysql_user'] = config.get('Mysql', 'user')
    conf['mysql_password'] = config.get('Mysql', 'password')
    conf['mysql_database'] = config.get('Mysql', 'database')
    conf['solr_host'] = config.get('Solr', 'host')
    #print conf
    return conf


def connect_to_mysql():
    conf = get_config()
    conn = pymysql.connect(host=conf['mysql_host'], port=conf['mysql_port'], user=conf['mysql_user'], db=conf['mysql_database'],passwd=conf['mysql_password'], charset='utf8')
    cur  = conn.cursor(pymysql.cursors.DictCursor)
    return conn, cur


def connect_to_solr():
    conf = get_config()
    solr = pysolr.Solr(conf['solr_host'], timeout=10)
    return solr


conn, cur = connect_to_mysql()
solr = connect_to_solr()
cause_dict = load_cause_dict()

def initial_resource():
    global conn
    global cur
    if not conn.ping():
        conn, cur = connect_to_mysql()
    return conn, cur


def overview(request):
    """
    INPUT: GET['district'] to indicate latest statistics data
    OUTPUT: [ { "district_ratio": "62.889126470000", "country_ratio": "27.132498119000", "cause": "急性支氣管炎及急性細支氣管炎" },...]
    """
    conn, cur = initial_resource()

    #print "req"
    result, response = [], []
    year = 2012
    district = request.GET.get('district', u"南投縣中寮鄉")
    sql = u"SELECT * FROM `dead_cause_statistics` WHERE `year`=%s AND (`district`='%s' OR `district` ='TAIWAN')" % (year, district)

    #sql = "SELECT * FROM  `dead_cause_statistics`  WHERE  `district` =  '南投縣中寮鄉'"
    #print sql
    cur.execute(sql)
    conn.commit()
    for row in cur:
        result.append(row)

    response = generate_ui_format(result)
    return HttpResponse(json.dumps(response, indent = 4, ensure_ascii=False).encode("utf-8")) 


def generate_ui_format(result):
    response = []
    tmp = {}
    for row in result:
        for key in row.keys():
            if key not in tmp and key[0] == "c":
                tmp[key] = {}
                tmp[key]['year'] = row['year']
            if key[0] == "c":
                tmp[key]["cause"] = cause_dict[key]
                tmp[key]["cause_code"] = key
                if row["district"] != "TAIWAN":
                    tmp[key]["district_ratio"] = float("%.2f" % (float(row[key]) * notation))
                else:
                    tmp[key]["country_ratio"] = float("%.2f" % (float(row[key]) * notation))

    for key, v in tmp.items():
        response.append(v)
    #print response
    sorted_response = sorted(response, key=lambda k: k['district_ratio'])
    return sorted_response[::-1]

def history(request):
    """
    INPUT: GET['district'], GET['cause']
    OUTPUT:[ { "district_ratio": "62.889126470000", "country_ratio": "27.132498119000", "cause": "急性支氣管炎及急性細支氣管炎", "year": "1991", "death_num": "123"}, ...]
    """

    conn, cur = initial_resource()
    inv_cause_dict = {v:k for k, v in cause_dict.items()}
    result, response = [], []
    district = request.GET.get('district', u"南投縣中寮鄉")
    cause = request.GET.get('cause', u"惡性腫瘤")
    cause_code = inv_cause_dict[cause]
    sql = u"SELECT %s, year, district FROM `dead_cause_statistics` WHERE `district`='%s' OR `district` ='TAIWAN'" % (cause_code, district)


    #sql = "SELECT * FROM  `dead_cause_statistics`  WHERE  `district` =  '南投縣中寮鄉'"
    #print sql
    cur.execute(sql)
    conn.commit()
    for row in cur:
        result.append(row)

    result_of_year = {}
    for row in result:
        if row['year'] not in result_of_year:
            result_of_year[row['year']] = []
        result_of_year[row['year']].append(row)
    #print len(result_of_year)
    #print result_of_year

    sql = u"SELECT %s, year, district  FROM `dead_cause` WHERE `district`='%s'" % (cause_code, district)
    #print sql
    cur.execute(sql)

    conn.commit()
    death_dict = {}
    for row in cur:
        death_dict[row['year']] = {}
        for k, v in row.items():
            death_dict[row['year']][k] = v
    #print death_dict
    #print len(death_dict)
    response = []
    for year in range(1991, 2013):
        tmp_dict = {}
        if year not in result_of_year:
            continue
        for data_dict in result_of_year[year]:
            if data_dict['district'] == "TAIWAN":
                tmp_dict['country_ratio'] = data_dict[cause_code] * notation
            else:
                tmp_dict['district_ratio'] = data_dict[cause_code] * notation
        tmp_dict["cause"] = cause_dict[cause_code]
        tmp_dict["year"] = year
        #if cause_code not in death_dict[year]:
        #    continue
        try:
            tmp_dict["death_num"] = death_dict[year][cause_code]
        except:
            print "DATA MISSING"
            #print year, death_dict.keys()
        response.append(tmp_dict)

    sorted_response = sorted(response, key=lambda k: k['year'])
    return HttpResponse(json.dumps(sorted_response, indent = 4, ensure_ascii=False).encode("utf-8"))
