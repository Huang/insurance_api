from django.conf.urls import url

from chart import views

urlpatterns = [
    url(r'^_overview$', views.overview),
    url(r'^_history$', views.history),
]
