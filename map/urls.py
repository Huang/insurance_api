from django.conf.urls import url

from map import views

urlpatterns = [
    url(r'^_get_rank$', views.get_rank),
]
