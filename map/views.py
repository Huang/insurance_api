# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
import pysolr
import json
import pymysql
import ConfigParser

# Create your views here.
__author__ = 'Chandler Huang <previa@gmail.com>'

cause_dict = {
    'c1': u"腸道感染症",
    'c2': u"結核病",
    'c3': u"敗血症",
    'c6': u"惡性腫瘤",
    'c8': u"貧血",
    'c9': u"糖尿病",
    'c15': u"高血壓性疾病",
    'c16': u"心臟疾病（高血壓性疾病除外）",
    'c17': u"腦血管疾病",
    'c18': u"動脈粥樣硬化",
    'c22': u"急性支氣管炎及急性細支氣管炎",
    'c28': u"慢性肝病及肝硬化",
    'c32': u"腎炎、腎病症候群及腎病變",
    'c38': u"事故傷害",
    'c39': u"蓄意自我傷害（自殺）",
    'c40': u"加害（他殺）",
    'c41': u"其他",
}


def get_config():
    config_filename = "settings_local"
    config = ConfigParser.ConfigParser()
    config.readfp(open(config_filename))
    conf = {}
    conf['mysql_host'] = config.get('Mysql', 'host')
    conf['mysql_port'] = int(config.get('Mysql', 'port'))
    conf['mysql_user'] = config.get('Mysql', 'user')
    conf['mysql_password'] = config.get('Mysql', 'password')
    conf['mysql_database'] = config.get('Mysql', 'database')
    conf['solr_host'] = config.get('Solr', 'host')
    #print conf
    return conf


def connect_to_mysql():
    conf = get_config()
    conn = pymysql.connect(host=conf['mysql_host'], port=conf['mysql_port'], user=conf['mysql_user'], db=conf['mysql_database'],passwd=conf['mysql_password'], charset='utf8')
    cur  = conn.cursor(pymysql.cursors.DictCursor)
    return conn, cur


def connect_to_solr():
    conf = get_config()
    solr = pysolr.Solr(conf['solr_host'], timeout=10)
    return solr


conn, cur = connect_to_mysql()
solr = connect_to_solr()


def initial_resource():
    global conn
    global cur
    if not conn.ping():
        conn, cur = connect_to_mysql()
    return conn, cur


def get_nearby_district(center, radius):
    print "get_nearby_district"
    conn, cur = initial_resource()

    q = "*:*"
    params = {"fq": "{!bbox}", "pt": center, "sfield": "latlng", "d": str(float(radius) / 1000), "rows": 500}
    docs = solr.search(q, **params)
    result = []
    for doc in docs:
        #print doc
        print "~~~"
        result.append(doc['district'])
    return result

def get_rank(request):
    print "get_rank"
    conn, cur = initial_resource()

    """
    INPUT: center, radius, cause
    OUTPUT:
    [
          {
                  "year": 1991,
                  "data": [
                            {
                                        "district_name": "臺北市松山區",
                                        "level": 5
                                      },
                            {
                                        "district_name": "臺北市中山區",
                                        "level": 4
                                      },
                            {
                                        "district_name": "臺北市中正區",
                                        "level": 3
                                      },
                            {
                                        "district_name": "臺北市內湖區",
                                        "level": 2
                                      },
                            {
                                        "district_name": "臺北市士林區",
                                        "level": 1
                                      }
                          ]
                },
          {
                  "year": 1992,
                  "data": [
                            {
                                        "district_name": "臺北市松山區",
                                        "level": 4
                                      },
                            {
                                        "district_name": "臺北市中山區",
                                        "level": 3
                                      },
                            {
                                        "district_name": "臺北市中正區",
                                        "level": 1
                                      },
                            {
                                        "district_name": "臺北市內湖區",
                                        "level": 2
                                      },
                            {
                                        "district_name": "臺北市士林區",
                                        "level": 5
                                      }
                          ]
                }
    ]
    """
    inv_cause_dict = {v:k for k, v in cause_dict.items()}
    result, response = [], []
    district = request.GET.get('district', u"南投縣中寮鄉")
    cause = request.GET.get('cause', u"惡性腫瘤")
    cause_code = inv_cause_dict[cause]

    center = request.GET.get("c", "25.0696929,121.5477232")
    radius = request.GET.get("r", 5000)

    districts = get_nearby_district(center, radius)
    """
    [u"臺北市士林區", u"臺北市內湖區", ... ]
    """

    dict_data_by_year = {}
    for district in districts:
        #print cause_code, district
        sql = u"SELECT %s as level, year, district FROM `dead_cause_map` WHERE `district`='%s'" % (cause_code, district)
        #print sql
        cur.execute(sql)
        conn.commit
        for row in cur:
            if row['year'] not in dict_data_by_year:
                dict_data_by_year[row['year']] = {}
            dict_data_by_year[row['year']][row["district"]] = row["level"]

    """
    {
        1991: {
                "臺北市松山區": 4,
                "臺北市中山區": 3,
                ...
              },
        1992: {
                ...
              }
    }
    """

    #print dict_data_by_year
    response = []
    for year, district_value_dict in dict_data_by_year.items():
        tmp_dict = {}
        for district, value in district_value_dict.items():
            if year not in tmp_dict:
                tmp_dict['year'] = year
            if 'data' not in tmp_dict:
                tmp_dict['data'] = []
            tmp_dict['data'].append({"district_name":district, "level":value})
        response.append(tmp_dict)

    return HttpResponse(json.dumps(response, indent = 4, ensure_ascii=False).encode("utf-8"))
